const express = require('express');
const fs = require('fs');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        const files = [];
        db.getData().then(files => {
             const promises = files.map(file => {
                 return new Promise((res, rej) => {
                    fs.readFile(file, (err, data) => {
                        if(err) {
                            rej(err);
                        } else {
                            res(JSON.parse(data));
                        }
                    });
                 });
            });
                return Promise.all(promises);
        }).then(result => {
            res.send(result);
        });
    });

    router.post('/', (req, res) => {
        const message = req.body;
        db.addMessage(message).then(result => {
            res.send(result)
        });
    });

    // router.get('/:messageDate', (req, res) => {
    //     res.send(db.getMessage(req.params.messageDate));
    // });

    return router;
};

module.exports = createRouter;