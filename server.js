const express = require('express');
const db = require('./dbMessage');
const messages = require('./app/messages');
const app = express();

const port = 8000;

app.use(express.json());

app.use('/messages', messages(db));

app.listen(port);

