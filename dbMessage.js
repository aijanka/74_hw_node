const fs = require('fs');
const path = "./messages";


module.exports = {
    getData: () => {
        return new Promise((resolve, reject) => {
            let filesArray = [];
            fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    filesArray.push(path + '/' + file);
                });
                const fiveFiles = filesArray.slice(-5);

                if(err) {
                    reject(err)
                } else {
                    resolve(fiveFiles);
                }

            })
        });


    },
    addMessage: (message) => {
        message.date = new Date();
        console.log(message);
        return new Promise((resolve, reject) => {
            fs.writeFile(`./messages/${message.date}.json`, message, err => {
                if(err) {
                    reject(err);
                } else {
                    resolve(message);
                }
            })
        });
    },
    // getMessage: date => {
    //     fs.readdir(path, (err, files) => {
    //         files.find(file => file === date);
    //     })
    // }

}